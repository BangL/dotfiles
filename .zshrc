#
# User configuration sourced by interactive shells
#

# cancel if non-interactive shell
[[ $- != *i* ]] && return

# Define zim location
export ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim

# Start zim
[[ -s ${ZIM_HOME}/init.zsh ]] && source ${ZIM_HOME}/init.zsh

# extend path variable with local user bin folder
export PATH=$HOME/.local/bin:$PATH

# Set nano as default editor
export EDITOR=nano
export VISUAL=$EDITOR

# define default ssh key
export SSH_KEY_PATH=$HOME/.ssh/rsa_id

# always use less instead of more
alias more="less"

# some aliases
alias cp="cp -aiv"
alias df="df -h"
alias du="du -h"
alias free="free -m"
alias tgz="tar -pczf"
alias diff="diff --color=auto"
alias ls="ls -vphF --color=auto --show-control-chars --group-directories-first"
alias ll="ls -lavphF --color=auto --show-control-chars --group-directories-first"

# run neofetch if installed
command -v neofetch >/dev/null 2>&1 && neofetch
